#!/usr/bin/python3

from sys import exit
import argparse
from os import mkdir
from shutil import rmtree
from os.path import join as pathjoin
from os.path import basename
from subprocess import run
from time import time, process_time

from slowbeast.parsers.llvm.parser import Parser as LLVMParser
from slowbeast.symexe.symbolicexecution import SEOptions
from slowbeast.util.debugging import set_debugging, dbg, print_stdout, print_stderr

def err(msg):
    print_stderr(msg, color="RED")
    exit(1)

def createArgParser():
    parser = argparse.ArgumentParser()
    parser.add_argument('prog', nargs='+', help='program to be analyzed')
    parser.add_argument('-dbg', action='store_true', help='write debugging messages')
    parser.add_argument('-lang', action='store', help='Specify the input'
                        'language: C, LLVM (forces the file to be compiled to LLVM if'
                        'lang is LLVM and input file is C)')
    parser.add_argument('-entry', default='main', help='entry function')
    parser.add_argument('-out-dir', default='sb-out', help='Directory for output files')
    parser.add_argument('-interactive', action='store_true', default=False, help='Introspect the run of the algorithm')
    parser.add_argument('-no-output', action='store_true', default=False, help='Genereate no output (other than stdout/stderr)')
    parser.add_argument('-verbose', '-v', action='store_true', dest='verbose',
                        default=False, help='Genereate verbose output')
    parser.add_argument('-verbose-verbose', '-vv', action='store_true', dest='verbose_v',
                        default=False, help='Genereate verbose lvl 2 output')
    parser.add_argument('-verbose-lvl', '-vlvl', type=int, dest='verbose_lvl',
                        default=1, help='The level of verbose output (the higher, the more verbose)')
    parser.add_argument('-dbgv', action='store_true', help='Shortcut for -dbg -v')
    parser.add_argument('-dbgvv', action='store_true', help='Shortcut for -dbg -vv')
    parser.add_argument('-no-tests', action='store_true', help='Genereate no test files')
    parser.add_argument('-all-tests', action='store_true', default=False, help='Genereate tests for all paths (not only the erroneous)')
    parser.add_argument('-pointer-bitwidth', action='store', type=int, help='Set the bitwidth of pointers')
    parser.add_argument('-concretize-nondet', action='store_true', default=False,
                        help='Use random value for nondet values, thus follow one random path during execution.')
    parser.add_argument('-uninitialized-nondet', action='store_true', default=False,
                        help='Use nondet value for reads from uninitialized memory (instead of issuing an error).')
    parser.add_argument('-dump', action='store_true', default=False, help='dump the program after parsing')
    parser.add_argument('-parse-only', action='store_true', help='only parse program and dump it (for debugging)')
    parser.add_argument('-error-fn', action='store', help='Error function (calling this function amounts to calling assert(false)')
    parser.add_argument('-kind', action='store_true', help='Use k-induction in SE (implies SE)')
    parser.add_argument('-kind-noinv', action='store_true',
                        help='Use k-induction without (internal) invariant generation')
    parser.add_argument('-kind-naive', action='store_true',
                        help='Use naive k-induction on CFG without any improvements')
    parser.add_argument('-kind-naive-impr', action='store_true',
                        help='Use improved naive k-induction')
    parser.add_argument('-kindse', action='store_true',
                        help='Use symbolic execution with k-induction and loop folding.')
    parser.add_argument('-kindse-til', action='store_true', help='Target towards induction is the last element in KindSE')
    parser.add_argument('-kindse-sis', action='store_true', help='Use simple starting inductive sets')
    parser.add_argument('-kind-step', type=int,
                        help='Set the step for k-induction, positive number is a number of basic blocks by which to extend in each iteration, 0 is extend the paths until a join, -1 is extend the paths until a branch (default)', default=-1)
    parser.add_argument('-bse', action='store_true', help='Do backward symbolic execution')
    parser.add_argument('-bse-add-unwind-inv', action='store_true',
                        help='Add invariants obtained from succesfull unwinding of a path (default: false)')
    parser.add_argument('-bself', action='store_true', help='Do backward symbolic execution with loops folding')
    parser.add_argument('-bself-til', action='store_true', help='Target towards induction is the last element in BSELF')
    parser.add_argument('-bself-union-extensions-thr', action='store',
                        default=None,
                        help='Union extensions of inductive sequences if '
                             'their number is at least this threshold '
                             '(0 = always union, default=None)')
    parser.add_argument('-bself-no-union-matched', action='store_true',
                        help='Do not union the sets matched in the stored inductive sequences '
                             'when getting starting inductive sequences')
    parser.add_argument('-se', action='store_true', default=False,
                        help='Perform symbolic execution (default).')
    parser.add_argument('-stateful-se', action='store_true', default=False,
                        help='Perform stateful symbolic execution (experimental).')
    parser.add_argument('-future-se', action='store_true', default=False,
                        help='Perform symbolic execution with on-demand subprograms search (experimental).')
    parser.add_argument('-se-step', default='instr',
                        help='Set what is an execution step, one of: block, instr (block = execute the whole blocks instead of single instructions in one step.')
    parser.add_argument('-se-exit-on-error', action='store_true',
                        help='Terminate symbolic execution after hitting the first error.')
    parser.add_argument('-se-replay-errors', action='store_true',
                        help='Confirm errors by replaying them without symbolic values')
    parser.add_argument('-se-incremental-solving', action='store_true',
                        help='Use incremental SMT solving')
    parser.add_argument('-ai', action='store_true', default=False,
                        help='Perform abstract interpretation.')
   #parser.add_argument('-ai-domain', action='store', default=True,
   #                    help='Perform abstract interpretation.')
    #parser.add_argument('-bmc', action='store_true', help='Perform bounded model checking.')

    return parser

def parseArgs():
    parser = createArgParser()
    args = parser.parse_args()

    valid_step = ['block', 'instr']

    if not args.se_step in valid_step:
        err("Invalid -step argument, must be one of: {0}, got '{1}'".format(valid_step, args.step))

    return args

def print_stats(engine):
    if hasattr(engine, 'executor'):
        executor = engine.executor()
        print_stdout(
            "Executed steps till branch: {0}".format(
                executor.getExecStepNum()),
            color='CYAN')
        print_stdout(
            "Executed instructions: {0}".format(
                executor.getExecInstrNum()),
            color='CYAN')
        print_stdout(
            "Executed branch instructions: {0}".format(
                executor.stats.branchings),
            color='CYAN')
        print_stdout(
            "Number of forks on branches: {0} (forked on {1}% of branches)".format(
                executor.stats.branch_forks,
                0 if executor.stats.branchings == 0 else
                100 *
                float(
                    executor.stats.branch_forks) /
                executor.stats.branchings),
            color='CYAN')
        # this includes e.g. forks on assertions/memory resolution/etc.
        print_stdout(
            "Number of all forks: {0} (from {1} calls ({2}%) to fork())".format(
                executor.stats.forks,
                executor.stats.fork_calls,
                0 if executor.stats.fork_calls == 0 else
                100 *
                float(
                    executor.stats.forks) /
                executor.stats.fork_calls),
            color='CYAN')


    print_stdout(
        "Executed paths: {0}".format(
            engine.stats.paths),
        color='CYAN')
    print_stdout(
        "Paths that reached exit: {0}".format(
            engine.stats.exited_paths),
        color='CYAN')
    print_stdout(
        "Paths that abnormally terminated: {0}".format(
            engine.stats.terminated_paths),
        color='CYAN')
    print_stdout(
        "Killed paths: {0}".format(engine.stats.killed_paths), color='CYAN')
    print_stdout(
        "Found errors: {0}".format(
            engine.stats.errors),
        color='CYAN')

class TestCaseGenerator:
    def __init__(self, outdir='sb-out', alltests=False):
        self._outputdir = outdir
        self._alltests = alltests

    def _openfile(self, path):
        return open(pathjoin(self._outputdir, path), 'w')

    def processErrorState(self, fl, state):
        fl.write(state.get_error().descr())
        fl.write('\n')
        fl.write('\n')
        fl.write("Nondeterministic values:\n")
        inpvec = state.input_vector()
        for var, val in zip(state.nondets(), inpvec):
            # dump as unsigned and signed
            fl.write("  {0} -> {1} ({2})\n".format(var, val,
                                                   (val.value() - (1 <<
                                                                      val.bitwidth()))))
        fl.write('\n')
        state.dump(stream=fl)

    def processState(self, state):
        assert not state.is_ready()

        if not self._alltests and state.exited():
            return

        filename=str(state.get_id())
        if state.has_error():
            filename+= ".err"
        elif state.was_killed():
            filename+= ".killed"
        elif state.is_terminated():
            filename+= ".abort"
        filename+=".test"

        with self._openfile(filename) as fl:
            fl.write(str(state.status()))
            fl.write('\n')
            if state.has_error():
                self.processErrorState(fl, state)
            else:
                fl.write('\n')
                state.dump(stream=fl)

class OutputsHandler:
    def __init__(self, testgen, outdir):
        # test generator
        self.testgen = testgen
        # where can the algorithm dump debugging data
        self.outdir = outdir
        # stream for logging
        #self.logstream = logstream

def sort_input_files(files):
    c_files = []
    llvm_files = []

    for f in files:
        if f.endswith('.c') or f.endswith('.i'):
            c_files.append(f)
        elif f.endswith('.bc') or f.endswith('.ll'):
            llvm_files.append(f)
        else:
            err("Unknown file: {0}".format(f))

    return c_files, llvm_files

def _run(cmd):
    dbg("RUN: {0}".format(" ".join(cmd)))

    cp = run(cmd)
    if cp.returncode != 0:
        err("Failed running: {0}".format(" ".join(cmd)))

def compile_c(path, outp=None):
    dbg("Compiling {0}".format(path))

    if outp is None:
        outp = pathjoin('/tmp/', basename(path) + '.bc')

    cmd = ['clang', '-emit-llvm', '-c', '-o', outp, path]
    _run(cmd)
    dbg("Compiled to {0}".format(outp))

    return outp

def link_llvm(paths, outp=None, outd='/tmp/'):
    dbg("Linking {0}".format(paths))

    if outp is None:
        outp = pathjoin(outd, 'code.bc')

    cmd = ['llvm-link', '-o', outp] + paths
    _run(cmd)

    dbg("Linked files to {0}".format(outp))

    return outp

def opt(path, opts=[], outp=None, outd='/tmp/'):
    dbg("Optimizing {0}".format(path))

    if outp is None:
        outp = pathjoin(outd, 'optcode.bc')

    cmd = ['opt', '-o', outp, path] + opts
    _run(cmd)

    dbg("Optimized files to {0}".format(outp))

    return outp

def compile_and_link(files, outd):
    c_files, llvm_files = sort_input_files(files)
    if c_files:
        for f in c_files:
            llvm_files.append(compile_c(f))

    assert len(llvm_files) > 0, "No input files"
    if len(llvm_files) > 1:
        bitcode = link_llvm(llvm_files, outd=outd)
    else:
        bitcode = llvm_files[0]

    return bitcode

def setup_debugging(args):
    if args.dbgvv:
        args.dbg = True
        args.verbose_lvl = max(3, args.verbose_lvl)
    if args.dbgv:
        args.dbg = True
        args.verbose_lvl = max(2, args.verbose_lvl)
    if args.verbose_v:
        args.verbose_lvl = max(3, args.verbose_lvl)
    if args.verbose:
        args.verbose_lvl = max(2, args.verbose_lvl)
    if args.dbg:
        set_debugging(args.verbose_lvl)

def main():
    # we do not use threads...
    from sys import setswitchinterval
    setswitchinterval(100)

    args = parseArgs()

    setup_debugging(args)

    if args.pointer_bitwidth:
        dbg(f"Setting pointer bitwidth to {args.pointer_bitwidth}")
        # NOTE: we must do this before building the IR
        from slowbeast.ir.types import sb_set_pointer_width
        sb_set_pointer_width(args.pointer_bitwidth)

    parser = None
    if args.lang:
        lang = args.lang.lower()
    else:
        lang = 'llvm'

    if lang == 'c':
        from slowbeast.parsers.c.parser import Parser as CParser
        parser = CParser()
        code = args.prog
    elif lang == 'llvm':
        from slowbeast.parsers.llvm.parser import Parser as LLVMParser
        error_funs = args.error_fn or None
        parser = LLVMParser(error_funs.split(',') if error_funs else None)
        code = compile_and_link(args.prog, args.out_dir)
    try:
        P = parser.parse(code)
    except FileNotFoundError as e:
        err(str(e))

    if not P:
        err("Failed parsing the code")

    if args.parse_only:
        P.dump()
        exit(0)

    if args.dump:
        P.dump()

    entry = P.fun(args.entry)
    if not entry:
        print("Entry function not found: {0}".format(args.entry))
        exit(1)

    testgen=None
    if not args.no_output:
        try:
            mkdir(args.out_dir)
        except OSError:
            print("The output dir exists, overwriting it")
            rmtree(args.out_dir)
            mkdir(args.out_dir)

        with open('{0}/program.txt'.format(args.out_dir), 'w') as f:
            P.dump(f)

        if not args.no_tests:
            testgen = TestCaseGenerator(args.out_dir, args.all_tests)

    ohandler = OutputsHandler(testgen,
                              None if args.no_output else args.out_dir)

    P.set_entry(entry)

    if args.bself:
        args.bse = True
    if args.kindse:
        args.kind = True
    if args.kind or args.bse: # kind implies se
        args.se = True

    if args.ai and args.se:
        err("Can run only one technique")
    if not (args.ai or args.se):
        args.se = True # no argument means SE

    if args.ai:
        opts = SEOptions() # FIXME
        from slowbeast.ai.abstractinterpreter import AbstractInterpreter
        interpreter = AbstractInterpreter(P, ohandler)
    elif args.kind:
        from slowbeast.kindse import KindSEOptions
        opts = KindSEOptions()

        if args.kindse:
            from slowbeast.kindse.kindse import KindSEOptions
            opts = KindSEOptions()
            if args.kindse_til:
                opts.target_is_whole_seq = False
            if args.kindse_sis:
                opts.simple_sis = True
            from slowbeast.kindse.kindse import KindSE as KindSymbolicExecutor
        elif args.kind_naive:
            from slowbeast.kindse.naive.naivekindse import KindSymbolicExecutor
        elif args.kind_naive_impr:
            from slowbeast.kindse.naive.naiveimprkindse import KindSymbolicExecutor
        else: # the default: if args.kind_noinv
            from slowbeast.kindse.legacy.noinvkindse import KindSymbolicExecutor
            # NOTE: now, -kind is the same as -bse. We must implement -bse
            # with precondition computation of every single step
        interpreter = KindSymbolicExecutor(P, ohandler, opts)
    elif args.bse:
        from slowbeast.bse.bself import BSELF, BSELFOptions
        opts = BSELFOptions()
        if args.bse and not args.bself:
            opts.fold_loops = False
        if args.bse_add_unwind_inv:
            opts.add_unwind_invariants = True
        if args.bself_til:
            opts.target_is_whole_seq = False
        if args.bself_union_extensions_thr is not None:
            opts.union_extensions_threshold = int(args.bself_union_extensions_thr)
        if args.bself_no_union_matched:
            assert opts.union_matched == True
            opts.union_matched = False
        interpreter = BSELF(P, ohandler, opts)
    elif args.se:
        opts = SEOptions()

        opts.replay_errors = args.se_replay_errors
        opts.exit_on_error = args.se_exit_on_error
        opts.interactive = args.interactive
        opts.incremental_solving = args.se_incremental_solving

        if args.se_step == 'block':
            opts.setBlockStep()

        if args.stateful_se:
            assert not args.future_se, "Not compatible at this moment"
            from slowbeast.symexe.statefulsymbolicexecution import StatefulSymbolicExecutor as SymbolicExecutor
        elif args.future_se:
            assert not args.stateful_se, "Not compatible at this moment"
            from slowbeast.symexe.futuresymbolicexecution import FutureSymbolicExecutor as SymbolicExecutor
        else:
            from slowbeast.symexe.symbolicexecution import SymbolicExecutor
        interpreter = SymbolicExecutor(P, ohandler, opts)

    opts.concretize_nondets = args.concretize_nondet
    opts.uninit_is_nondet = args.uninitialized_nondet

    # user-defined error functions (issue error when called)
    if args.error_fn:
        opts.error_funs = args.error_fn.split(',')

    walltime, cputime = time(), process_time()
    try:
        interpreter.run()
        print_stats(interpreter)
    except KeyboardInterrupt:
        print_stdout("Interrupted...")
        print_stats(interpreter)
    finally:
        wt = time()-walltime
        ct = process_time()-cputime
        print_stdout(f"wall-time: {wt}, cpu-time: {ct}", color="gray")

    exit(0)

if __name__ == "__main__":
    main()
